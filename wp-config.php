<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'amjenton' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o~tU<Mrdj7RU{-}ou1`L}@;<7iI3elM%[[lP(a2=ub~8#_F*4LHr<?Q-+u1zwK-M' );
define( 'SECURE_AUTH_KEY',  '|N?9xVjU%WJ6aP5!ynZ)J:G5uix)t&Va0gX*3obG#{s-;.O!qjp`RL>Y,8<cLkF&' );
define( 'LOGGED_IN_KEY',    '}|,xzL]aY!zUBHF)%T[caZB$69Io &&M_,nx4f?#qX1@C9NkEK1=QBmtsQpN.}w{' );
define( 'NONCE_KEY',        'g~i`  4,p EviMq:)NIozmMPf3%W0h|P|tm4E,vSs(5?UX$wl&fhf_56va>Ff/}=' );
define( 'AUTH_SALT',        'UIR_Y?*HL~ueTe8CJ;_mkyOvT# Mc_/#`F`tQ|C1!ciG/Zry.Lux;^QP6mBO)uCD' );
define( 'SECURE_AUTH_SALT', 'OxQ^%iA`i~o.&a}6!*w2[.[:P2%SFdzGo[fJ]U5x MoZu~]5!:.@4S)70)gI8zJ$' );
define( 'LOGGED_IN_SALT',   'z(n?X@aQ4oD.Aahj[omZ~Tmdd{9,H(GBU=xEmbeCzE@enq3s|[`(9icvI^Wmv%Z4' );
define( 'NONCE_SALT',       '#c%dAGVlQ_hjC,(^l66uW4N%/fx.Y$Pn4Va~%iETh44cVM?*k1<*30QJo5vMc.m-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
