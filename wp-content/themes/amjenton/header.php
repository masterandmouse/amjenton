<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package AM Jenton
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-96x96.png" sizes="96x96">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<header id="secondary_header">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="header_info">
						<?php dynamic_sidebar( 'header_info' ); ?>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="social_links">
						<?php dynamic_sidebar( 'social_links' ); ?>
					</div>
				</div>
			</div>
		</div>
	</header>
	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<nav id="site-navigation" class="main-navigation navbar navbar-default" role="navigation">
				<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'amjenton' ); ?></a>
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main">
	                    <span class="sr-only"><?php _e('Toggle navigation', 'amjenton'); ?></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <div class="site-branding navbar-brand">
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
					</div>
	            </div>

	            <div class="collapse navbar-collapse" id="navbar-collapse-main">
		            <ul class="nav navbar-nav navbar-right">
			            <?php if( has_nav_menu( 'primary' ) ) :
				            wp_nav_menu( array(
			                        'theme_location'  => 'primary',
			                        'container'       => false,
			                        //'menu_class'      => 'nav navbar-nav',//  'nav navbar-right'
			                        'walker'          => new Bootstrap_Nav_Menu(),
			                        'fallback_cb'     => null,
					                'items_wrap'      => '%3$s',// skip the containing <ul>
			                    )
			                );
		                else :
			                wp_list_pages( array(
					                'menu_class'      => 'nav navbar-nav',//  'nav navbar-right'
					                'walker'          => new Bootstrap_Page_Menu(),
					                'title_li'        => null,
				                )
			                );
			            endif; ?>
		            </ul>
		            <?php //get_search_form(); ?>
	            </div><!-- /.navbar-collapse -->

			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		
