<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package AM Jenton
 */

?>

	</div><!-- #content -->

	<footer class="footer_row_1">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 footer_1_1"><?php dynamic_sidebar( 'footer_1_1' ); ?></div>
				<div class="col-sm-4 footer_1_2"><?php dynamic_sidebar( 'footer_1_2' ); ?></div>
			</div>
		</div>
	</footer>
	<footer class="footer_row_2">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-3 footer_2_1"><?php dynamic_sidebar( 'footer_2_1' ); ?></div>
				<div class="col-sm-6 col-md-3 footer_2_2"><?php dynamic_sidebar( 'footer_2_2' ); ?></div>
				<div class="col-sm-6 col-md-3 footer_2_3"><?php dynamic_sidebar( 'footer_2_3' ); ?></div>
				<div class="col-sm-6 col-md-3 footer_2_4"><?php dynamic_sidebar( 'footer_2_4' ); ?></div>
			</div>
		</div>
	</footer>
	<footer class="footer_row_3">
		<div class="container">
			<div id="colophon" class="site-footer" role="contentinfo">
				<div class="row">
					<div class="col-md-5 footer_3_1">
						<?php dynamic_sidebar( 'footer_menu' ); ?>
					</div>
					<div class="col-md-7 footer_3_2">
						<div class="site-info">
							<p>Copyright &#169; <?php echo date("Y") ?> <?php bloginfo( 'name' ); ?>. All Rights Reserved. Website Design by <a href="http://masterandmouse.com/" target="_blank">Master &amp; Mouse</a>.</p>
						</div><!-- .site-info -->
					</div>
				</div>
			</div><!-- #colophon -->
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
