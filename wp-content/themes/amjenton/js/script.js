jQuery(document).ready(function($) {

	$('.smooth_scroll').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      var the_top = target.offset().top - 50;
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: the_top
	        }, 1000);
	        return false;
	      }
	    }
	});

});