<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package AM Jenton
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function amjenton_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'amjenton_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function amjenton_jetpack_setup
add_action( 'after_setup_theme', 'amjenton_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function amjenton_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function amjenton_infinite_scroll_render
