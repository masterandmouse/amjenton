<?php

namespace OTGS\Toolset\Views\Controller\Cache\Meta;

use OTGS\Toolset\Views\Model\Wordpress\Transient;
use OTGS\Toolset\Views\Controller\Cache\Meta\Base;

/**
 * Meta cache controller.
 *
 * @since 2.8.1
 */
abstract class ManagerBase {

	const LIMIT = 512;

	const VISIBLE_KEY = '';
	const HIDDEN_KEY = '';

	/**
	 * @var Transient
	 */
	protected $transient_manager = null;

	/**
	 * @var Base
	 */
	protected $meta_cache = null;

	/**
	 * Constructor
	 *
	 * @param Transient $transient_manager
	 * @since 2.8.1
	 */
	public function __construct( Transient $transient_manager ) {
		$this->transient_manager = $transient_manager;
	}

	/**
	 * Initialize this controller.
	 *
	 * @param Base $meta_cache
	 * @since 2.8.1
	 */
	public function initialize( Base $meta_cache ) {
		$this->meta_cache = $meta_cache;

		$this->add_hooks();
	}

	/**
	 * Register API hooks to get, generate or delete the cache.
	 *
	 * @since 2.8.1
	 */
	abstract protected function add_hooks();

	/**
	 * Get the cache for visible fields.
	 *
	 * @return array
	 * @since 2.8.1
	 */
	public function get_visible_cache() {
		$meta_cache = $this->meta_cache;
		return $this->transient_manager->get_transient( $meta_cache::VISIBLE_KEY );
	}

	/**
	 * Get existing or generate anew cache for visible meta fields.
	 *
	 * @return array
	 * @since 2.8.1
	 */
	public function get_or_generate_visible_cache() {
		$cache = $this->get_visible_cache();

		if ( false !== $cache ) {
			return $cache;
		}

		return $this->generate_visible_cache();
	}

	/**
	 * Generate a new cache for visible meta fields,
	 * each subclass must provide this method.
	 *
	 * @return array
	 * @since 2.8.1
	 */
	abstract public function generate_visible_cache();

	/**
	 * Set the cache for visible fields.
	 *
	 * @param mixed $cache
	 * @return bool
	 * @since 2.8.1
	 */
	public function set_visible_cache( $cache ) {
		$meta_cache = $this->meta_cache;
		return $this->transient_manager->set_transient( $meta_cache::VISIBLE_KEY, $cache );
	}

	/**
	 * Delete the cache for visible fields.
	 *
	 * @return bool
	 * @since 2.8.1
	 */
	public function delete_visible_cache() {
		$meta_cache = $this->meta_cache;
		return $this->transient_manager->delete_transient( $meta_cache::VISIBLE_KEY );
	}

	/**
	 * Get the cache for hidden fields.
	 *
	 * @return array
	 * @since 2.8.1
	 */
	public function get_hidden_cache() {
		$meta_cache = $this->meta_cache;
		return $this->transient_manager->get_transient( $meta_cache::HIDDEN_KEY );
	}

	/**
	 * Get existing or generate anew cache for hidden meta fields.
	 *
	 * @return array
	 * @since 2.8.1
	 */
	public function get_or_generate_hidden_cache() {
		$cache = $this->get_hidden_cache();

		if ( false !== $cache ) {
			return $cache;
		}

		return $this->generate_hidden_cache();
	}

	/**
	 * Generate a new cache for hidden meta fields,
	 * each subclass must provide this method.
	 *
	 * @return array
	 * @since 2.8.1
	 */
	abstract public function generate_hidden_cache();

	/**
	 * Set the cache for hidden fields.
	 *
	 * @param mixed $cache
	 * @return bool
	 * @since 2.8.1
	 */
	public function set_hidden_cache( $cache ) {
		$meta_cache = $this->meta_cache;
		return $this->transient_manager->set_transient( $meta_cache::HIDDEN_KEY, $cache );
	}

	/**
	 * Delete the cache for hidden fields.
	 *
	 * @return bool
	 * @since 2.8.1
	 */
	public function delete_hidden_cache() {
		$meta_cache = $this->meta_cache;
		return $this->transient_manager->delete_transient( $meta_cache::HIDDEN_KEY );
	}

}
