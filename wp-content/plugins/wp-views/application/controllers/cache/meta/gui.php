<?php

namespace OTGS\Toolset\Views\Controller\Cache\Meta;

/**
 * GUI for the settings about the meta fields cache.
 *
 * @since 2.8.1
 */
class Gui {

	const CRON = 'cron';
	const MANUAL = 'manual';

	/**
	 * Initialize this GUI by adding a section to the right tab of the Toolset Settings.
	 *
	 * @since 2.8.1
	 */
	public function initialize() {
		add_filter( 'toolset_filter_toolset_register_settings_front-end-content_section', array( $this, 'render_options' ), 75 );
	}

	/**
	 * Render the options for this set of settings.
	 *
	 * @param array $sections
	 * @return array
	 * @since 2.8.1
	 */
	public function render_options( $sections ) {
		$settings = \WPV_Settings::get_instance();
		ob_start();
		?>
		<h3><?php
		/* translators: Title of the setting section about the plugin generated cache for custom fields keys */
		echo esc_html( __( 'Cache for custom fields', 'wpv-views' ) );
		?></h3>
		<div class="toolset-advanced-setting">
			<p>
				<?php
				/* translators: Explanation for the setting about how to invalidate the internal cache for custom field keys */
				echo esc_html( __( 'Views stores a cache for custom fields keys for internal usage. Generating this cache can be expensive on large sites, so you might want to keep this cache until you manually clear it.', 'wpv-views' ) ); ?>
			</p>
			<ul>
				<li>
					<label>
						<input id="wpv-manage-meta-transient-method-<?php echo esc_attr( self::CRON ); ?>"
							type="radio"
							autocomplete="off"
							name="wpv-manage-meta-transient-method"
							<?php checked( $settings->manage_meta_transient_method, self::CRON ); ?>
							value="<?php echo esc_attr( self::CRON ); ?>" />
						<?php
						/* translators: Label of the option to invalidate the cache for custom fields when a new field is created. */
						echo esc_html( __( 'Toolset Views will manage this cache and clear it automatically', 'wpv-views' ) );
						?>
					</label>
				</li>
				<li>
					<label>
						<input id="wpv-manage-meta-transient-method-<?php echo esc_attr( self::MANUAL ); ?>"
							type="radio"
							autocomplete="off"
							name="wpv-manage-meta-transient-method"
							<?php checked( $settings->manage_meta_transient_method, self::MANUAL ); ?>
							value="<?php echo esc_attr( self::MANUAL ); ?>" />
						<?php
						/* translators: Label of the option to invalidate the cache for custom fields only manually. */
						echo esc_html( __( 'Keep this cache until I clear it manually here', 'wpv-views' ) );
						?>
					</label>
				</li>
			</ul>
			<p>
				<button class="button-secondary js-wpv-manage-meta-transient-method-manual-fire">
					<?php
					/* translators: Label of the butto to manually invalidate the custom fields cacke */
					echo esc_html( __( 'Clear custom fields cache now', 'wpv-views' ) );
					?>
				</button>
			</p>
		</div>
		<?php
		$section_content = ob_get_clean();

		$sections['wpv-manage-meta-transient-method'] = array(
			'slug' => 'wpv-manage-meta-transient-method',
			/* translators: Title of the setting section about the plugin generated cache */
			'title' => __( 'Cache', 'wpv-views' ),
			'content' => $section_content,
		);
		return $sections;
	}

}
