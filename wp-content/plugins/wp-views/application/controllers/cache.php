<?php

namespace OTGS\Toolset\Views\Controller;

/**
 * Plugin cache controller.
 *
 * @since 2.8.1
 */
class Cache {

	/**
	 * @var \OTGS\Toolset\Views\Controller\Cache\Meta\Post
	 */
	private $postmeta_cache = null;

	/**
	 * @var \OTGS\Toolset\Views\Controller\Cache\Meta\Term
	 */
	private $termmeta_cache = null;

	/**
	 * @var \OTGS\Toolset\Views\Controller\Cache\Meta\User
	 */
	private $usermeta_cache = null;

	/**
	 * @var \OTGS\Toolset\Views\Controller\Cache\Meta\Gui
	 */
	private $cache_gui = null;

	/**
	 * Constructor.
	 *
	 * @param \OTGS\Toolset\Views\Controller\Cache\Meta\Post $postmeta_cache
	 * @param \OTGS\Toolset\Views\Controller\Cache\Meta\Term $termmeta_cache
	 * @param \OTGS\Toolset\Views\Controller\Cache\Meta\User $usermeta_cache
	 * @param \OTGS\Toolset\Views\Controller\Cache\Meta\Gui $cache_gui
	 * @since 2.8.1
	 */
	public function __construct(
		\OTGS\Toolset\Views\Controller\Cache\Meta\Post $postmeta_cache,
		\OTGS\Toolset\Views\Controller\Cache\Meta\Term $termmeta_cache,
		\OTGS\Toolset\Views\Controller\Cache\Meta\User $usermeta_cache,
		\OTGS\Toolset\Views\Controller\Cache\Meta\Gui $cache_gui
	) {
		$this->postmeta_cache = $postmeta_cache;
		$this->termmeta_cache = $termmeta_cache;
		$this->usermeta_cache = $usermeta_cache;
		$this->cache_gui = $cache_gui;
	}

	/**
	 * Initialize the object.
	 *
	 * @since 2.8.1
	 */
	public function initialize() {
		$this->postmeta_cache->initialize();
		$this->termmeta_cache->initialize();
		$this->usermeta_cache->initialize();
		$this->cache_gui->initialize();
	}

}
